import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: require('@/components/all').default
    },
    {
      path: '/angi',
      name: 'angi',
      props: true,
      component: require('@/components/main').default
    }
  ]
})
